
from typing import Optional, Tuple

from qgis.core import *
from qgis.gui import *

from pygsf.inspections.errors import *
from pygsf.geometries.lines import *

def qgs_project_xy(
        x: float,
        y: float,
        srcCrs: QgsCoordinateReferenceSystem = None,
        destCrs:Optional[QgsCoordinateReferenceSystem] = None
) -> Tuple[float, float]:
    """
    Project a pair of x-y coordinates to a new projection.
    If the source/destination CRS is not provided, it will be set to EPSG 4236 (WGS-84).

    :param x: the x coordinate.
    :param y: the y coordinate.
    :param srcCrs: the source coordinate.
    :param destCrs: the destination coordinate.
    :return: the projected x-y coordinates.
    """

    if not srcCrs:
        srcCrs = QgsCoordinateReferenceSystem(
            4326,
            QgsCoordinateReferenceSystem.EpsgCrsId)

    if not destCrs:
        destCrs = QgsCoordinateReferenceSystem(
            4326,
            QgsCoordinateReferenceSystem.EpsgCrsId)

    coordinate_transform = QgsCoordinateTransform(
        srcCrs,
        destCrs,
        QgsProject.instance())

    qgs_pt = coordinate_transform.transform(
        x,
        y)

    x, y = qgs_pt.x(), qgs_pt.y()

    return x, y


def project_qgs_point(qgsPt, srcCrs, destCrs):
    return QgsCoordinateTransform(srcCrs, destCrs, QgsProject.instance()).transform(qgsPt)


def project_line_2d(srcLine, srcCrs, destCrs):
    destLine = Ln()
    for pt in srcLine._pts:
        srcPt = QgsPointXY(pt._x, pt._y)
        destPt = project_qgs_point(srcPt, srcCrs, destCrs)
        destLine = destLine.add_pt(Point(destPt.x(), destPt.y()))

    return destLine
