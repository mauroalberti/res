import numbers
from xml.dom.minidom import parse

from enum import Enum, auto
from math import asin

from gst.pygsf.inspections.errors import *
from gst.pygsf.profiles.profilers import *
from gst.pygsf.profiles.profiletraces import *

from gst.qgis.rasters import *
from gst.qgis.lines import *
from gst.qgis.projections import *
from gst.plots.parameters import TOPOPROF_MAX_PTS_NUM_DEFAULT


class ProfileSource(Enum):
    """
    The profile source type.
    """

    UNDEFINED  = auto()
    LINE_LAYER = auto()
    DIGITATION = auto()
    POINT_LIST = auto()
    GPX_FILE   = auto()

# 20220803: possibly no longer used
class GPXElevationUsage(Enum):
    """
    The profile source type.
    """

    NOT_USED = auto()
    USE_WITH_DEMS = auto()
    ONLY_TO_USE = auto()

