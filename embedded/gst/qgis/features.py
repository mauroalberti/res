
from qgis.core import *

from gst.pygsf.geometries.lines import *


def extract_from_linestring_3d(
        linestring: QgsLineString
) -> Ln:

    coords = []

    for pt in linestring:
        coords.append([pt.x, pt.y, pt.z])

    return Ln(coords)


def extract_from_multilinestring_3d(
        multilinestring: QgsMultiLineString
) -> MultiLine:

    lines = []

    for line in multilinestring:

        lines.append(
            extract_from_linestring_3d(line)
        )

    return MultiLine(lines)
