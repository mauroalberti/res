
from typing import Tuple, List, Union

import os


def try_find_files_in_folder(
        folder_path: str
) -> Tuple[bool, Union[str, List[str]]]:

    try:

        file_paths = sorted(filter(lambda fl: os.path.isfile, os.listdir(folder_path)))
        return True, file_paths

    except Exception as e:

        return False, str(e)


def try_find_tif_files_in_folder(
        folder_path: str
) -> Tuple[bool, Union[str, List[str]]]:
    """
    Returns sorted paths of tif files in folder
    """

    try:

        success, result = try_find_files_in_folder(folder_path)
        if not success:
            msg = result
            return False, msg

        tif_file_paths = sorted(filter(lambda fl: fl.lower().endswith(".tif"), result))

        return True, tif_file_paths

    except Exception as e:

        return False, str(e)


if __name__ == '__main__':

    pass

