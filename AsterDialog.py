
from qgis.core import QgsCoordinateReferenceSystem
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtWidgets import *

from gst.filesystem.files import *
from gst.yaml.io import *
from gst.qt.tools import *
from gst.qt.messages import *

from .config.general_params import *

from .LithologicalIndices import *


litho_parameters = (
    ("Ferric iron index", ("Rowan and Mars, 2003",)),             #  0
    ("Ferrous iron index", ("Rowan et al., 2005",)),              #  1
    ("Gossan index", ("Velosky et al., 2003",)),                  #  2
    ("Generalized hydroxyl-alteration", ()),                      #  3
    ("Propylitic index", ("Rowan et al., 2005",)),                #  4
    ("Phyllic index", ("Rowan and Mars, 2003",                    #  5
                       "Rowan et al., 2005",                      #  6
                       "Ninomiya, 2003a")),                       #  7
    ("Argillic index", ("Mars and Rowan, 2006",                   #  8
                        "Ninomiya, 2003a")),                      #  9
    ("Calcite index", ("Rowan and Mars, 2003",                    # 10
                       "Ninomiya, 2003b")),                       # 11
    ("Quartz index (QI)", ("Ninomiya et al., 2005",               # 12
                           "Rockwell and Hofstra, 2008")),        # 13
    ("Silica index (SI)", ("Ninomiya and Fu, 2002",               # 14
                           "Rowan et al., 2006")),                # 15
    ("Carbonate index (CI)", ("Ninomiya and Fu, 2002",)),         # 16
    ("Mafic index (MI)", ("Ninomiya et al., 2005",)),             # 17
    ("Mafic index corrected (MIc)", ("Ninomiya et al., 2005",)),  # 18
    ("Ultramafic index (UMI)", ("Rowan et al., 2005",)),          # 19
)

litho_choices = []
for litho_index, references in litho_parameters:
    if references:
        for reference in references:
            litho_choices.append(f"{litho_index} - {reference}")
    else:
        litho_choices.append(litho_index)


class AsterMainWidget(QWidget):

    bfp_calc_update = pyqtSignal()

    def __init__(self,
                 tool_nm,
                 canvas,
                 plugin_qaction
                 ):

        super(AsterMainWidget, self).__init__()

        self.tool_nm = tool_nm
        self.canvas, self.plugin = canvas, plugin_qaction

        self.param_funcs = (
            save_ferric_iron_index,  #  0
            save_ferrous_iron_index,  #  1
            save_gossan_index,  #  2
            save_gener_hydroxil_alter_index,  #  3
            save_propylitic_index,  #  4
            save_phyllic_index_rm2003,  #  5
            save_phyllic_index_ra2005,  #  6
            save_phyllic_index_n2003a,  #  7
            save_argillic_index_mr2006,  #  8
            save_argillic_index_n2003a,  #  9
            save_calcite_index_rm2003,  # 10
            save_calcite_index_n2003b,  # 11
            save_quartz_index_na2005,  # 12
            save_quartz_index_rh2008,  # 13
            save_silica_index_1,  # 14
            save_silica_index_2,  # 15
            save_carbonate_index,  # 16
            save_mafic_index_na2005,  # 17
            save_mafic_index_corrected_na2005,  # 18
            save_ultramafic_index_ra2005,  # 19
        )

        self.setup_gui()

    def setup_gui(self):

        dialog_layout = QVBoxLayout()
        self.main_widget = QTabWidget()
        self.main_widget.addTab(self.setup_input_tab(), "Input")
        self.main_widget.addTab(self.setup_indices_calc_tab(), "Geological indices")
        self.main_widget.addTab(self.setup_output_tab(), "Output")
        self.main_widget.addTab(self.setup_help_tab(), "Help")

        dialog_layout.addWidget(self.main_widget)
        self.setLayout(dialog_layout)                    
        self.adjustSize()                       
        self.setWindowTitle('{} - {}'.format(plugin_nm, self.tool_nm))

    def setup_input_tab(self):
        
        widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.setup_aster_folder())
        layout.addWidget(self.setup_aster_bands())
        widget.setLayout(layout)
        return widget

    def setup_indices_calc_tab(self):

        widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.setup_indices_parameters())
        widget.setLayout(layout)
        return widget

    def setup_output_tab(self):

        widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.setup_output_folder())
        layout.addWidget(self.setup_calculate_results())
        layout.addWidget(self.setup_write_log())
        widget.setLayout(layout)
        return widget

    def setup_help_tab(self):

        qwdtHelp = QWidget()
        qlytHelp = QVBoxLayout()

        qtbrHelp = QTextBrowser(qwdtHelp)
        url_path = "file:///{}/help/help_aster.html".format(os.path.dirname(__file__))
        qtbrHelp.setSource(QUrl(url_path))
        qtbrHelp.setSearchPaths(['{}/help'.format(os.path.dirname(__file__))])
        qlytHelp.addWidget(qtbrHelp)

        qwdtHelp.setLayout(qlytHelp)

        return qwdtHelp

    def set_output_dest_folder(self):

        dirName = QFileDialog.getExistingDirectory(
            self,
            self.tr("Select output directory"),
            QDir.currentPath(),
            QFileDialog.ShowDirsOnly |
            QFileDialog.ReadOnly
        )

        if not dirName:
            return

        self.TifOutputFolder_linedit.setText(dirName)

    def setup_output_folder(self):

        main_groupbox = QGroupBox(self.tr("Output tif folder"))

        main_layout = QGridLayout()

        self.TifOutputFolder_linedit = QLineEdit()
        main_layout.addWidget(self.TifOutputFolder_linedit, 0, 1)

        self.TifOutputFolder_QPushButton = QPushButton(self.tr("Choose directory"))
        self.TifOutputFolder_QPushButton.clicked.connect(self.set_output_dest_folder)

        main_layout.addWidget(self.TifOutputFolder_QPushButton, 0, 2)

        main_groupbox.setLayout(main_layout)

        return main_groupbox

    def setup_calculate_results(self):

        main_groupbox = QGroupBox(self.tr("Calculate results"))

        main_layout = QVBoxLayout()

        self.calculateCompositeImage = QCheckBox("Calculate composite image")
        self.calculateCompositeImage.setChecked(True)
        main_layout.addWidget(self.calculateCompositeImage)

        """
        self.addResultsToProject = QCheckBox("Add results to project")
        main_layout.addWidget(self.addResultsToProject)
        """

        self.CalculateResults_QPushButton = QPushButton(self.tr("Calculate indices"))
        self.CalculateResults_QPushButton.clicked.connect(self.calculate_indices)

        main_layout.addWidget(self.CalculateResults_QPushButton)

        main_groupbox.setLayout(main_layout)

        return main_groupbox

    def setup_write_log(self):

        main_groupbox = QGroupBox(self.tr("Log"))

        main_layout = QVBoxLayout()

        self.log_area = QTextBrowser()
        main_layout.addWidget(self.log_area)

        main_groupbox.setLayout(main_layout)

        return main_groupbox

    def define_if_chosen(self,
                         index:str
                         ) -> bool:

        for ndx in range(self.index_layout.count()):

            check_box = self.index_layout.itemAt(ndx).widget()

            check_box_text = check_box.text()

            if check_box_text == index:

                if check_box.isChecked():
                    return True

        return False

    def create_composite_3N21_image(self,
                                    src_folder_path: str,
                                    dest_folder_path: str
                                    ) -> Error:

        try:

            B1_flnm = self.bands_paths_linedits["B1"].text()
            B2_flnm = self.bands_paths_linedits["B2"].text()
            B3N_flnm = self.bands_paths_linedits["B3N"].text()

            if not (B1_flnm and B2_flnm and B3N_flnm):
                return Error(
                    True,
                    caller_name(),
                    Exception("Not all required bands available"),
                    traceback.format_exc()
                )

            B1_flpth = os.path.join(
                src_folder_path,
                B1_flnm
            )

            B2_flpth = os.path.join(
                src_folder_path,
                B2_flnm
            )

            B3N_flpth = os.path.join(
                src_folder_path,
                B3N_flnm
            )

            # B1

            result, err = read_raster_band(
                    raster_source=B1_flpth)

            if err:
                return err

            B1_geotransform, B1_projection, _, B1_array = result

            # B2

            result, err = read_raster_band(
                    raster_source=B2_flpth)

            if err:
                return err

            B2_geotransform, B2_projection, _, B2_array = result

            if B2_projection != B1_projection:
                return Error(
                    True,
                    caller_name(),
                    Exception(f"Band 2 proj is {B2_projection} while band 1 proj is {B1_projection}"),
                    traceback.format_exc()
                )

            if B2_array.shape != B1_array.shape:
                return Error(
                    True,
                    caller_name(),
                    Exception(f"Band 2 shape is {B2_array.shape} while band 1 shape is {B1_array.shape}"),
                    traceback.format_exc()
                )

            # B3N

            result, err = read_raster_band(
                    raster_source=B3N_flpth)

            if err:
                return err

            B3N_geotransform, B3N_projection, _, B3N_array = result

            if B3N_projection != B1_projection:
                return Error(
                    True,
                    caller_name(),
                    Exception(f"Band 3N proj is {B3N_projection} while band 1 proj is {B1_projection}"),
                    traceback.format_exc()
                )

            if B3N_array.shape != B1_array.shape:
                return Error(
                    True,
                    caller_name(),
                    Exception(f"Band 3N shape is {B3N_array.shape} while band 1 shape is {B1_array.shape}"),
                    traceback.format_exc()
                )

            tMergedImageFlnm = "Composite_3N21.tif"

            return compose_raster_image(
                dst_file_path=os.path.join(
                    dest_folder_path,
                    tMergedImageFlnm),
                array_red=B3N_array,
                array_green=B2_array,
                array_blue=B1_array,
                geotransform=B1_geotransform,
                projection=B1_projection
            )

        except Exception as e:

            return Error(
                True,
                caller_name(),
                e,
                traceback.format_exc()
            )


    def calculate_indices(self):

        source_folder_path = self.AsterSourceFolder_linedit.text()
        output_folder_path = self.TifOutputFolder_linedit.text()

        if self.calculateCompositeImage.isChecked():

            err = self.create_composite_3N21_image(
                src_folder_path=source_folder_path,
                dest_folder_path=output_folder_path
            )

            if err:
                error_qt(
                    parent=self,
                    header="res",
                    msg=repr(err)
                )

        chosen_parameters = []
        for index in litho_choices:

            isChosen = self.define_if_chosen(index)

            if isChosen:
                chosen_parameters.append(index)

        if not chosen_parameters:
            warn_qt(
                parent=self,
                header="res",
                msg="No chosen indices")

            return

        for param in chosen_parameters:

            ndx = litho_choices.index(param)

            func = self.param_funcs[ndx]

            err = func(
                bands_paths_linedits=self.bands_paths_linedits,
                src_folder_path=source_folder_path,
                dest_folder_path=output_folder_path
            )

            if err:
                print(f"Error: {repr(err)}")
            else:
                print(f"Band calculated and saved")

        info_qt(
            None,
            "res",
            "Calculations completed"
        )

    def set_aster_source_folder(self):

        dirName = QFileDialog.getExistingDirectory(
            self,
            self.tr("Select ASTER directory"),
            QDir.currentPath(),
            QFileDialog.ShowDirsOnly |
            QFileDialog.ReadOnly
        )

        if not dirName:
            return

        self.AsterSourceFolder_linedit.setText(dirName)

    def search_for_bands(self):

        self.clean_aster_band_paths()

        success, result = try_find_tif_files_in_folder(
            folder_path=self.AsterSourceFolder_linedit.text()
        )

        if not success:
            msg = result
            error_qt(
                window_title="ASTER tif search",
                text="Exception",
                informative_text=msg
            )
            return

        tifs_paths = result
        for tif_path in tifs_paths:
            band = tif_path.split(".")[-2].upper().replace("IMAGEDATA", "B")
            if band in self.bands_paths_linedits:
                self.bands_paths_linedits[band].setText(tif_path)

    def setup_aster_folder(self):

        main_groupbox = QGroupBox(self.tr("Source ASTER folder"))
        
        main_layout = QGridLayout()

        self.AsterSourceFolder_linedit = QLineEdit()
        main_layout.addWidget(self.AsterSourceFolder_linedit, 0, 1)
        self.AsterSourceFolder_linedit.textChanged.connect(self.search_for_bands)
        self.AsterSourceFolder_QPushButton = QPushButton(self.tr("Choose directory"))
        self.AsterSourceFolder_QPushButton.clicked.connect(self.set_aster_source_folder)

        main_layout.addWidget(self.AsterSourceFolder_QPushButton, 0, 2)

        main_groupbox.setLayout(main_layout)
              
        return main_groupbox

    def setup_aster_bands(self):
        
        main_groupbox = QGroupBox(self.tr("Source ASTER bands"))
        
        main_layout = QFormLayout()

        self.bands_paths_linedits = dict()
        band_names = []
        for i in range(1, 15):
            if i == 3:
                for char in ('N', 'B'):
                    band_names.append(f"B{i}{char}")
            else:
                band_names.append(f"B{i}")

        for band_name in band_names:
            self.bands_paths_linedits[band_name] = QLineEdit()
            main_layout.addRow(band_name, self.bands_paths_linedits[band_name])

        main_groupbox.setLayout(main_layout)
                 
        return main_groupbox

    def clean_aster_band_paths(self):

        for band_name in self.bands_paths_linedits:
            self.bands_paths_linedits[band_name].setText("")

    def setup_indices_parameters(self):

        group_box = QGroupBox(self.tr("Calculate mineralogical-lithological parameter"))

        self.index_layout = QVBoxLayout()

        for litho_choice in litho_choices:
            self.index_layout.addWidget(QCheckBox(litho_choice))

        group_box.setLayout(self.index_layout)

        return group_box

    def get_project_crs(self) -> QgsCoordinateReferenceSystem:

        return self.canvas.mapSettings().destinationCrs()

    def get_project_crs_authid(self) -> str:

        return self.get_project_crs().authid()

    def get_project_crs_descr(self) -> str:

        return self.get_project_crs().description()

    def get_project_crs_long_descr(self) -> str:

        return "{} - {}".format(
            self.get_project_crs_authid(),
            self.get_project_crs_descr()
        )


