
from typing import List

import numbers

from gst.pygsf.inspections.errors import *


def field_values(
    layer,
    curr_field_ndx: numbers.Integral
):

    values = []

    if layer.selectedFeatureCount() > 0:
        features = layer.selectedFeatures()
    else:
        features = layer.getFeatures()

    for feature in features:
        values.append(feature.attributes()[curr_field_ndx])

    return values


def vect_attrs(
    layer,
    field_list
):
    """
    Probably to deprecate in favour of 'lyr_attrs'
    """

    if layer.selectedFeatureCount() > 0:
        features = layer.selectedFeatures()
    else:
        features = layer.getFeatures()

    provider = layer.dataProvider()
    field_indices = [provider.fieldNameIndex(field_name) for field_name in field_list]

    # retrieve (selected) attributes features
    data_list = []
    for feature in features:
        attrs = feature.fields().toList()
        data_list.append([feature.attribute(attrs[field_ndx].name()) for field_ndx in field_indices])

    return data_list


def lyr_attrs(layer, fields: List) -> List:
    """
    Get attributes from layer based on provided field names list.

    :param layer: the source layer for attribute extraction.
    :param fields: a list of field names for attribute extraction.
    :return: list of table values.
    """

    if layer.selectedFeatureCount() > 0:
        features = layer.selectedFeatures()
    else:
        features = layer.getFeatures()

    provider = layer.dataProvider()
    field_indices = [provider.fieldNameIndex(field_name) for field_name in fields]

    # retrieve selected features with relevant attributes

    rec_list = []

    for feature in features:

        attrs = feature.fields().toList()

        # creates feature attribute list

        feat_list = []
        for field_ndx in field_indices:
            feat_list.append(feature.attribute(attrs[field_ndx].name()))

        # add to result list

        rec_list.append(feat_list)

    return rec_list

