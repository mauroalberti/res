
import os
import traceback

import numpy as np

from gst.pygsf.inspections.errors import *
from gst.pygsf.inspections.functions import *
from gst.io.rasters.gdal_io import *


tFerricIronIndexFlnm = "FerricIronNdx.tif"
tFerrousIronIndexFlnm = "FerrousIronNdx.tif"
tGossanIndexFlnm = "GossanNdx.tif"
tSilicaIndexNF2002Flnm = "SilicaIndexNF2002.tif"
tSilicaIndexRA2005Flnm = "SilicaIndexRA2005.tif"
tCarbonateIndexNF2002Flnm = "CarbonateIndexNF2002.tif"
tMaficIndexNA2005Flnm = "MaficIndexNA2005.tif"
tMaficIndexCorrectedNA2005Flnm = "MaficIndexCorrectedNA2005.tif"
tPropyliticIndexNA2005Flnm = "PropyliticIndexRA2005.tif"
tPhyllicIndexRM2003Flnm = "PhyllicIndexRM2003.tif"
tPhyllicIndexRA2005Flnm = "PhyllicIndexRA2005.tif"
tPhyllicIndexN2003aFlnm = "PhyllicIndexN2003a.tif"
tArgillicIndexMR2006Flnm = "ArgillicIndexMR2006.tif"
tArgillicIndexN2003aFlnm = "ArgillicIndexN2003a.tif"
tCalciteIndexRM2003Flnm = "CalciteIndexRM2003.tif"
tCalciteIndexN2003bFlnm = "CalciteIndexN2003b.tif"
tUltramaficIndexRA2005Flnm = "UltramaficIndexRA2005.tif"
tGenerHydroxilAlterIndexFlnm = "GenerHydroxilAlterIndex.tif"
tQuartzIndexNA2005Flnm = "QuartzIndexNA2005.tif"
tQuartzIndexRH2008Flnm = "QuartzIndexRH2008.tif"


def save_argillic_index_mr2006(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B4_flnm = bands_paths_linedits["B4"].text()
    B6_flnm = bands_paths_linedits["B6"].text()
    B5_flnm = bands_paths_linedits["B5"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B4_flnm),
        num2_band_pth=os.path.join(src_folder_path, B6_flnm),
        denom_band_pth=os.path.join(src_folder_path, B5_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tArgillicIndexMR2006Flnm),
        formula='plus'
    )


def save_phyllic_index_n2003a(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B4_flnm = bands_paths_linedits["B4"].text()
    B7_flnm = bands_paths_linedits["B7"].text()
    B6_flnm = bands_paths_linedits["B6"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B4_flnm),
        num2_band_pth=os.path.join(src_folder_path, B7_flnm),
        denom_band_pth=os.path.join(src_folder_path, B6_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tPhyllicIndexN2003aFlnm),
        formula='mult'
    )


def save_ferric_iron_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B1_flnm = bands_paths_linedits["B1"].text()
        B2_flnm = bands_paths_linedits["B2"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B2_flnm),
            denom_band_pth=os.path.join(src_folder_path, B1_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tFerricIronIndexFlnm)
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_gossan_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B4_flnm = bands_paths_linedits["B4"].text()
        B2_flnm = bands_paths_linedits["B2"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B4_flnm),
            denom_band_pth=os.path.join(src_folder_path, B2_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tGossanIndexFlnm),
            num_zoom=2
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_silica_index_1(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B13_flnm = bands_paths_linedits["B13"].text()
        B12_flnm = bands_paths_linedits["B12"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B13_flnm),
            denom_band_pth=os.path.join(src_folder_path, B12_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tSilicaIndexNF2002Flnm)
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_silica_index_2(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B14_flnm = bands_paths_linedits["B14"].text()
        B12_flnm = bands_paths_linedits["B12"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B14_flnm),
            denom_band_pth=os.path.join(src_folder_path, B12_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tSilicaIndexRA2005Flnm)
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def calculate_carbonate_index(
        bands_paths_linedits,
        src_folder_path: str,
) -> Tuple[Union[type(None), Tuple[np.ndarray, GeoTransform, numbers.Integral]], Error]:

    try:

        B13_flnm = bands_paths_linedits["B13"].text()
        B14_flnm = bands_paths_linedits["B14"].text()

        return calculate_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B13_flnm),
            denom_band_pth=os.path.join(src_folder_path, B14_flnm),
        )

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_carbonate_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str,
) -> Error:

    try:

        B13_flnm = bands_paths_linedits["B13"].text()
        B14_flnm = bands_paths_linedits["B14"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B13_flnm),
            denom_band_pth=os.path.join(src_folder_path, B14_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tCarbonateIndexNF2002Flnm),
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_mafic_index_na2005(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str,
) -> Error:

    try:

        B12_flnm = bands_paths_linedits["B12"].text()
        B13_flnm = bands_paths_linedits["B13"].text()

        return save_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B12_flnm),
            denom_band_pth=os.path.join(src_folder_path, B13_flnm),
            dest_gtif_flpth=os.path.join(dest_folder_path, tMaficIndexNA2005Flnm),
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def calculate_mafic_index_na2005(
        bands_paths_linedits,
        src_folder_path: str,
) -> Tuple[Union[type(None), Tuple[np.ndarray, GeoTransform, numbers.Integral]], Error]:

    try:

        B12_flnm = bands_paths_linedits["B12"].text()
        B13_flnm = bands_paths_linedits["B13"].text()

        return calculate_band_ratio(
            num_band_pth=os.path.join(src_folder_path, B12_flnm),
            denom_band_pth=os.path.join(src_folder_path, B13_flnm),
        )

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_mafic_index_corrected_na2005(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    result, err = calculate_mafic_index_na2005(
        bands_paths_linedits,
        src_folder_path=src_folder_path,
    )

    if err:
        return err

    mafic_index_array, mafic_gt, mafic_proj = result

    result, err = calculate_carbonate_index(
        bands_paths_linedits,
        src_folder_path=src_folder_path,
    )

    if err:
        return err

    carbonate_index_array, carbonate_gt, carbonate_proj = result

    if not np.allclose(carbonate_gt, mafic_gt):
        return Error(
            True,
            caller_name(),
            Exception(f"Carbonate geotransform is {carbonate_gt} while mafic geotransform is {mafic_gt}"),
            traceback.format_exc()
        )

    if carbonate_proj != mafic_proj:
        return Error(
            True,
            caller_name(),
            Exception(f"Carbonate projection is {carbonate_proj} while mafic projection is {mafic_proj}"),
            traceback.format_exc()
        )

    denominator_array = np.power(carbonate_index_array, 3)
    denominator_array = np.where(denominator_array == 0.0, np.nan, denominator_array)

    index_array = np.divide(mafic_index_array, denominator_array)

    return create_raster_grid(
        dst_file_path=os.path.join(dest_folder_path, tMaficIndexCorrectedNA2005Flnm),
        array=index_array,
        geotransform=carbonate_gt,
        projection=carbonate_proj
    )


def save_propylitic_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B7_flnm = bands_paths_linedits["B7"].text()
    B9_flnm = bands_paths_linedits["B9"].text()
    B8_flnm = bands_paths_linedits["B8"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B7_flnm),
        num2_band_pth=os.path.join(src_folder_path, B9_flnm),
        denom_band_pth=os.path.join(src_folder_path, B8_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tPropyliticIndexNA2005Flnm),
        formula='plus'
    )


def save_phyllic_index_rm2003(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B5_flnm = bands_paths_linedits["B5"].text()
    B7_flnm = bands_paths_linedits["B7"].text()
    B6_flnm = bands_paths_linedits["B6"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B5_flnm),
        num2_band_pth=os.path.join(src_folder_path, B7_flnm),
        denom_band_pth=os.path.join(src_folder_path, B6_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tPhyllicIndexRM2003Flnm),
        formula='plus'
    )


def save_phyllic_index_ra2005(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B4_flnm = bands_paths_linedits["B4"].text()
    B7_flnm = bands_paths_linedits["B7"].text()
    B6_flnm = bands_paths_linedits["B6"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B4_flnm),
        num2_band_pth=os.path.join(src_folder_path, B7_flnm),
        denom_band_pth=os.path.join(src_folder_path, B6_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tPhyllicIndexRA2005Flnm),
        formula='plus'
    )


def save_argillic_index_n2003a(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B4_flnm = bands_paths_linedits["B4"].text()
    B7_flnm = bands_paths_linedits["B7"].text()
    B5_flnm = bands_paths_linedits["B5"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B4_flnm),
        num2_band_pth=os.path.join(src_folder_path, B7_flnm),
        denom_band_pth=os.path.join(src_folder_path, B5_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tArgillicIndexN2003aFlnm),
        formula='mult'
    )


def save_calcite_index_rm2003(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B7_flnm = bands_paths_linedits["B7"].text()
    B9_flnm = bands_paths_linedits["B9"].text()
    B8_flnm = bands_paths_linedits["B8"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B7_flnm),
        num2_band_pth=os.path.join(src_folder_path, B9_flnm),
        denom_band_pth=os.path.join(src_folder_path, B8_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tCalciteIndexRM2003Flnm),
        formula='plus'
    )


def save_calcite_index_n2003b(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B6_flnm = bands_paths_linedits["B6"].text()
    B9_flnm = bands_paths_linedits["B9"].text()
    B8_flnm = bands_paths_linedits["B8"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B6_flnm),
        num2_band_pth=os.path.join(src_folder_path, B9_flnm),
        denom_band_pth=os.path.join(src_folder_path, B8_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tCalciteIndexN2003bFlnm),
        formula='mult'
    )


def save_ultramafic_index_ra2005(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    B12_flnm = bands_paths_linedits["B12"].text()
    B14_flnm = bands_paths_linedits["B14"].text()
    B13_flnm = bands_paths_linedits["B13"].text()

    return save_band_ratio_ternary(
        num1_band_pth=os.path.join(src_folder_path, B12_flnm),
        num2_band_pth=os.path.join(src_folder_path, B14_flnm),
        denom_band_pth=os.path.join(src_folder_path, B13_flnm),
        dest_gtif_flpth=os.path.join(dest_folder_path, tUltramaficIndexRA2005Flnm),
        formula='plus'
    )


def save_gener_hydroxil_alter_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B4_flnm = bands_paths_linedits["B4"].text()
        B5_flnm = bands_paths_linedits["B5"].text()
        B6_flnm = bands_paths_linedits["B6"].text()
        B7_flnm = bands_paths_linedits["B7"].text()

        B4_flpth = os.path.join(
            src_folder_path,
            B4_flnm
        )

        B5_flpth = os.path.join(
            src_folder_path,
            B5_flnm
        )

        B6_flpth = os.path.join(
            src_folder_path,
            B6_flnm
        )

        B7_flpth = os.path.join(
            src_folder_path,
            B7_flnm
        )

        # B4

        result, err = read_raster_band(
                raster_source=B4_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B4_geotransform, B4_projection, _, B4_array = result

        # B5

        result, err = read_raster_band(
                raster_source=B5_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B5_geotransform, B5_projection, _, B5_array = result

        if B5_projection != B4_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 5 proj is {B5_projection} while band 4 proj is {B4_projection}"),
                traceback.format_exc()
            )

        if B5_array.shape != B4_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 5 shape is {B5_array.shape} while band 4 shape is {B4_array.shape}"),
                traceback.format_exc()
            )

        # B6

        result, err = read_raster_band(
                raster_source=B6_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B6_geotransform, B6_projection, _, B6_array = result

        if B6_projection != B4_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 6 proj is {B6_projection} while band 4 proj is {B4_projection}"),
                traceback.format_exc()
            )

        if B6_array.shape != B4_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 6 shape is {B6_array.shape} while band 4 shape is {B4_array.shape}"),
                traceback.format_exc()
            )

        # B7

        result, err = read_raster_band(
                raster_source=B7_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B7_geotransform, B7_projection, _, B7_array = result

        if B7_projection != B4_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 7 proj is {B7_projection} while band 4 proj is {B4_projection}"),
                traceback.format_exc()
            )

        if B7_array.shape != B4_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 7 shape is {B7_array.shape} while band 4 shape is {B4_array.shape}"),
                traceback.format_exc()
            )

        # calculates denominator

        denominator_array = B5_array + B6_array + B7_array
        denominator_array = np.where(denominator_array == 0, np.nan, denominator_array)

        # calculate index array

        index_array = np.divide(B4_array * 3, denominator_array)

        return create_raster_grid(
            dst_file_path=os.path.join(
                dest_folder_path,
                tGenerHydroxilAlterIndexFlnm),
            array=index_array,
            geotransform=B4_geotransform,
            projection=B4_projection
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_quartz_index_na2005(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B10_flnm = bands_paths_linedits["B10"].text()
        B11_flnm = bands_paths_linedits["B11"].text()
        B12_flnm = bands_paths_linedits["B12"].text()

        B10_flpth = os.path.join(
            src_folder_path,
            B10_flnm
        )

        B11_flpth = os.path.join(
            src_folder_path,
            B11_flnm
        )

        B12_flpth = os.path.join(
            src_folder_path,
            B12_flnm
        )

        # B10

        result, err = read_raster_band(
                raster_source=B10_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B10_geotransform, B10_projection, _, B10_array = result

        # B11

        result, err = read_raster_band(
                raster_source=B11_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B11_geotransform, B11_projection, _, B11_array = result

        if B11_projection != B10_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 11 proj is {B11_projection} while band 10 proj is {B10_projection}"),
                traceback.format_exc()
            )

        if B11_array.shape != B10_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 11 shape is {B11_array.shape} while band 10 shape is {B10_array.shape}"),
                traceback.format_exc()
            )

        # B12

        result, err = read_raster_band(
                raster_source=B12_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B12_geotransform, B12_projection, _, B12_array = result

        if B12_projection != B10_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 12 proj is {B12_projection} while band 10 proj is {B10_projection}"),
                traceback.format_exc()
            )

        if B12_array.shape != B12_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 12 shape is {B12_array.shape} while band 10 shape is {B10_array.shape}"),
                traceback.format_exc()
            )

        # calculates denominator

        denominator_array = np.multiply(B10_array, B12_array)
        denominator_array = np.where(denominator_array == 0, np.nan, denominator_array)

        # calculate index array

        index_array = np.divide(np.square(B11_array), denominator_array)

        return create_raster_grid(
            dst_file_path=os.path.join(
                dest_folder_path,
                tQuartzIndexNA2005Flnm),
            array=index_array,
            geotransform=B10_geotransform,
            projection=B10_projection
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_quartz_index_rh2008(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B10_flnm = bands_paths_linedits["B10"].text()
        B11_flnm = bands_paths_linedits["B11"].text()
        B12_flnm = bands_paths_linedits["B12"].text()
        B13_flnm = bands_paths_linedits["B13"].text()

        B10_flpth = os.path.join(
            src_folder_path,
            B10_flnm
        )

        B11_flpth = os.path.join(
            src_folder_path,
            B11_flnm
        )

        B12_flpth = os.path.join(
            src_folder_path,
            B12_flnm
        )

        B13_flpth = os.path.join(
            src_folder_path,
            B13_flnm
        )

        # B10

        result, err = read_raster_band(
                raster_source=B10_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B10_geotransform, B10_projection, _, B10_array = result

        # B11

        result, err = read_raster_band(
                raster_source=B11_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B11_geotransform, B11_projection, _, B11_array = result

        if B11_projection != B10_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 11 proj is {B11_projection} while band 10 proj is {B10_projection}"),
                traceback.format_exc()
            )

        if B11_array.shape != B10_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 11 shape is {B11_array.shape} while band 10 shape is {B10_array.shape}"),
                traceback.format_exc()
            )

        # B12

        result, err = read_raster_band(
                raster_source=B12_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B12_geotransform, B12_projection, _, B12_array = result

        if B12_projection != B10_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 12 proj is {B12_projection} while band 10 proj is {B10_projection}"),
                traceback.format_exc()
            )

        if B12_array.shape != B10_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 12 shape is {B12_array.shape} while band 10 shape is {B10_array.shape}"),
                traceback.format_exc()
            )

        # B13

        result, err = read_raster_band(
                raster_source=B13_flpth)

        if err:
            msg = result
            return Error(
                True,
                caller_name(),
                Exception(msg),
                traceback.format_exc()
            )

        B13_geotransform, B13_projection, _, B13_array = result

        if B13_projection != B10_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 13 proj is {B13_projection} while band 10 proj is {B10_projection}"),
                traceback.format_exc()
            )

        if B13_array.shape != B10_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 13 shape is {B13_array.shape} while band 10 shape is {B10_array.shape}"),
                traceback.format_exc()
            )

        # calculates denominator

        denominator1_array = np.multiply(B10_array, B12_array)
        denominator1_array = np.where(denominator1_array == 0, np.nan, denominator1_array)

        B12_array = np.where(B12_array == 0, np.nan, B12_array)

        # calculate index array

        index1_array = np.divide(B11_array, denominator1_array)
        index2_array = np.divide(B13_array, B12_array)

        index_array = np.multiply(index1_array, index2_array)

        # save results

        return create_raster_grid(
            dst_file_path=os.path.join(
                dest_folder_path,
                tQuartzIndexRH2008Flnm),
            array=index_array,
            geotransform=B10_geotransform,
            projection=B10_projection
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def save_ferrous_iron_index(
        bands_paths_linedits,
        src_folder_path: str,
        dest_folder_path: str
) -> Error:

    try:

        B1_flnm = bands_paths_linedits["B1"].text()
        B2_flnm = bands_paths_linedits["B2"].text()
        B3N_flnm = bands_paths_linedits["B3N"].text()
        B5_flnm = bands_paths_linedits["B5"].text()

        B1_flpth = os.path.join(
            src_folder_path,
            B1_flnm
        )

        B2_flpth = os.path.join(
            src_folder_path,
            B2_flnm
        )

        B3N_flpth = os.path.join(
            src_folder_path,
            B3N_flnm
        )

        B5_flpth = os.path.join(
            src_folder_path,
            B5_flnm
        )

        result, err = read_raster_band(
                raster_source=B1_flpth)

        if err:
            return err

        # B1

        B1_geotransform, B1_projection, _, B1_array = result

        result, err = read_raster_band(
                raster_source=B2_flpth)

        if err:
            return err

        # B2

        B2_geotransform, B2_projection, _, B2_array = result

        if B2_projection != B1_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 2 proj is {B2_projection} while band 1 proj is {B1_projection}"),
                traceback.format_exc()
            )

        if B2_array.shape != B1_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 2 shape is {B2_array.shape} while band 1 shape is {B1_array.shape}"),
                traceback.format_exc()
            )

        result, err = read_raster_band(
                raster_source=B3N_flpth)

        if err:
            return err

        # B3N

        B3N_geotransform, B3N_projection, _, B3N_array = result

        if B3N_projection != B1_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 3N proj is {B3N_projection} while band 1 proj is {B1_projection}"),
                traceback.format_exc()
            )

        if B3N_array.shape != B1_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 3N shape is {B3N_array.shape} while band 1 shape is {B1_array.shape}"),
                traceback.format_exc()
            )

        # B5

        result, err = read_raster_band(
                raster_source=B5_flpth)

        if err:
            return err

        B5_geotransform, B5_projection, _, B5_array = result
        # from: https://stackoverflow.com/questions/13242382/resampling-a-numpy-array-representing-an-image
        # Joe Kington
        B5_array = scipy.ndimage.zoom(B5_array, 2, order=0)

        if B5_projection != B1_projection:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 5 proj is {B5_projection} while band 1 proj is {B1_projection}"),
                traceback.format_exc()
            )

        if B5_array.shape != B1_array.shape:
            return Error(
                True,
                caller_name(),
                Exception(f"Band 5 shape is {B5_array.shape} while band 1 shape is {B1_array.shape}"),
                traceback.format_exc()
            )

        B1_array = np.where(B1_array == 0, np.nan, B1_array)
        B2_array = np.where(B2_array == 0, np.nan, B2_array)
        B3N_array = np.where(B3N_array == 0, np.nan, B1_array)
        B5_array = np.where(B5_array == 0, np.nan, B2_array)

        div12_array = np.divide(B1_array, B2_array)
        div53_array = np.divide(B5_array, B3N_array)

        index_array = div53_array + div12_array

        return create_raster_grid(
            dst_file_path=os.path.join(
                dest_folder_path,
                tFerrousIronIndexFlnm),
            array=index_array,
            geotransform=B1_geotransform,
            projection=B1_projection
        )

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )
