
from scipy import ndimage

from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtCore import *

from gst.filesystem.files import *
from gst.io.rasters.gdal_io import *
from gst.qt.messages import *

from .config.general_params import *



class FiltersMainWidget(QWidget):

    bfp_calc_update = pyqtSignal()

    def __init__(self,
                 tool_nm,
                 canvas,
                 plugin_qaction
                 ):

        super(FiltersMainWidget, self).__init__()

        self.tool_nm = tool_nm
        self.canvas, self.plugin = canvas, plugin_qaction

        self.setup_gui()

    def setup_gui(self):

        dialog_layout = QVBoxLayout()
        self.main_widget = QTabWidget()
        self.main_widget.addTab(self.setup_io_tab(), "Input/Output")
        self.main_widget.addTab(self.setup_filters_tab(), "Filters")
        self.main_widget.addTab(self.setup_help_tab(), "Help")

        dialog_layout.addWidget(self.main_widget)
        self.setLayout(dialog_layout)                    
        self.adjustSize()                       
        self.setWindowTitle('{} - {}'.format(plugin_nm, self.tool_nm))

    def setup_io_tab(self):
        
        widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.setup_input_folder())
        layout.addWidget(self.setup_filters_output_folder())
        widget.setLayout(layout)
        return widget

    def setup_median_filter_parameters(self):

        filter_parameters_qgroupbox = QGroupBox()
        filter_parameters_qgroupbox.setTitle('Parameters')

        filter_parameters_qformlayout = QFormLayout()

        # Size

        self.median_filter_size_qspinbox = QSpinBox()
        self.median_filter_size_qspinbox.setMinimum(3)
        self.median_filter_size_qspinbox.setSingleStep(2)

        filter_parameters_qformlayout.addRow(
            "Size",
            self.median_filter_size_qspinbox
        )

        # Mode

        median_modes = [
            "reflect",
            "constant",
            "nearest",
            "mirror",
            "wrap"
        ]

        self.median_filter_mode_qcombobox = QComboBox()
        self.median_filter_mode_qcombobox.insertItems(
            0,
            median_modes
        )
        self.median_filter_mode_qcombobox.setCurrentText("constant")

        filter_parameters_qformlayout.addRow(
            "Mode",
            self.median_filter_mode_qcombobox
        )

        # Constant fill value

        self.median_filter_constant_fill_value_qdoublespinbox = QDoubleSpinBox()
        self.median_filter_constant_fill_value_qdoublespinbox.setMinimum(-9999999.9999)
        self.median_filter_constant_fill_value_qdoublespinbox.setValue(0.0)

        filter_parameters_qformlayout.addRow(
            "Constant fill value",
            self.median_filter_constant_fill_value_qdoublespinbox
        )

        # Finalizations

        filter_parameters_qgroupbox.setLayout(filter_parameters_qformlayout)

        return filter_parameters_qgroupbox

    def setup_median_filter_calculate(self):

        filter_calculate_qgroupbox = QGroupBox()
        filter_calculate_qgroupbox.setTitle('Calculations')

        filter_calculations_qvboxlayout = QVBoxLayout()

        # Calculate

        median_filter_calculate_qpushbutton = QPushButton("Calculate")
        median_filter_calculate_qpushbutton.clicked.connect(self.calculate_median_filter)

        filter_calculations_qvboxlayout.addWidget(median_filter_calculate_qpushbutton)

        # Finalizations

        filter_calculate_qgroupbox.setLayout(filter_calculations_qvboxlayout)

        return filter_calculate_qgroupbox

    def setup_median_filter_toolbox(self):

        median_filter_toolbox = QToolBox()

        median_filter_qwidget = QWidget()
        median_filter_qvboxlayout = QVBoxLayout()

        median_filter_qvboxlayout.addWidget(self.setup_median_filter_parameters())
        median_filter_qvboxlayout.addWidget(self.setup_median_filter_calculate())

        median_filter_qwidget.setLayout(median_filter_qvboxlayout)

        median_filter_toolbox.addItem(
            median_filter_qwidget,
            "Median filter"
        )

        return median_filter_toolbox

    def setup_filters_tab(self):

        section_filters_QWidget = QWidget()
        section_filters_layout = QVBoxLayout()

        section_filters_layout.addWidget(self.setup_median_filter_toolbox())

        section_filters_QWidget.setLayout(section_filters_layout)

        return section_filters_QWidget

    def setup_help_tab(self):

        qwdtHelp = QWidget()
        qlytHelp = QVBoxLayout()

        qtbrHelp = QTextBrowser(qwdtHelp)
        url_path = "file:///{}/help/help_aster.html".format(os.path.dirname(__file__))
        qtbrHelp.setSource(QUrl(url_path))
        qtbrHelp.setSearchPaths(['{}/help'.format(os.path.dirname(__file__))])
        qlytHelp.addWidget(qtbrHelp)

        qwdtHelp.setLayout(qlytHelp)

        return qwdtHelp

    def setup_filtered_images_output_folder(self):

        dirName = QFileDialog.getExistingDirectory(
            self,
            self.tr("Select output directory"),
            QDir.currentPath(),
            QFileDialog.ShowDirsOnly | QFileDialog.ReadOnly
        )

        if not dirName:
            return

        self.ImagesFilteredFolder_linedit.setText(dirName)

    def calculate_median_filter(self):

        tif_src_images_fldr_pth = self.ImagesSourceFolder_linedit.text()
        tif_dest_images_fldr_pth = self.ImagesFilteredFolder_linedit.text()

        median_filter_size = self.median_filter_size_qspinbox.value()
        median_filter_mode = self.median_filter_mode_qcombobox.currentText()
        median_filter_constant_fill_value = self.median_filter_constant_fill_value_qdoublespinbox.value()

        # Read images file list

        success, result = try_find_tif_files_in_folder(
            folder_path=tif_src_images_fldr_pth
        )

        if not success:
            msg = result
            error_qt(
                parent=self,
                header="Error with median filter calculation",
                msg=msg
            )
            return

        tif_names = result

        # Cycle on image files and filter and save each

        for tif_name in tif_names:

            # read band as numpy array

            tif_path = os.path.join(
                tif_src_images_fldr_pth,
                tif_name
            )
            result, err = read_raster_band(
                    raster_source=tif_path)

            if err:
                return err

            tif_geotransform, tif_projection, _, tif_array = result

            # calculate median

            filtered_array = ndimage.median_filter(
                input=tif_array,
                size=median_filter_size,
                mode=median_filter_mode,
                cval=median_filter_constant_fill_value
            )

            # save array as tif file

            dest_gtif_flpth = os.path.join(
                tif_dest_images_fldr_pth,
                tif_name
            )

            err = create_raster_grid(
                dst_file_path=dest_gtif_flpth,
                array=filtered_array,
                geotransform=tif_geotransform,
                projection=tif_projection
            )
            if err:
                return err

        info_qt(
            parent=self,
            header="Median filter calculation",
            msg="Completed"
        )

    def set_aster_source_folder(self):

        dirName = QFileDialog.getExistingDirectory(
            self,
            self.tr("Select ASTER directory"),
            QDir.currentPath(),
            QFileDialog.ShowDirsOnly |
            QFileDialog.ReadOnly
        )

        if not dirName:
            return

        self.ImagesSourceFolder_linedit.setText(dirName)

    def setup_input_folder(self):

        main_groupbox = QGroupBox(self.tr("Source images folder"))
        
        main_layout = QGridLayout()

        self.ImagesSourceFolder_linedit = QLineEdit()
        main_layout.addWidget(self.ImagesSourceFolder_linedit, 0, 1)
        #self.ImagesSourceFolder_linedit.textChanged.connect(self.search_for_bands)
        self.ImagesSourceFolder_QPushButton = QPushButton(self.tr("Choose directory"))
        self.ImagesSourceFolder_QPushButton.clicked.connect(self.set_aster_source_folder)

        main_layout.addWidget(self.ImagesSourceFolder_QPushButton, 0, 2)

        main_groupbox.setLayout(main_layout)
              
        return main_groupbox

    def setup_filters_output_folder(self):

        main_groupbox = QGroupBox(self.tr("Filtered output images folder"))

        main_layout = QGridLayout()

        self.ImagesFilteredFolder_linedit = QLineEdit()
        main_layout.addWidget(self.ImagesFilteredFolder_linedit, 0, 1)
        #self.ImagesFilteredFolder_linedit.textChanged.connect(self.search_for_bands)
        self.ImagesFilteredFolder = QPushButton(self.tr("Choose directory"))
        self.ImagesFilteredFolder.clicked.connect(self.setup_filtered_images_output_folder)

        main_layout.addWidget(self.ImagesFilteredFolder, 0, 2)

        main_groupbox.setLayout(main_layout)

        return main_groupbox

