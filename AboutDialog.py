
from qgis.PyQt.QtWidgets import QDialog, QVBoxLayout, QTextBrowser

from .config.general_params import *


class AboutDialog(QDialog):

    def __init__(self, version):

        super(AboutDialog, self).__init__()

        dialog_layout = QVBoxLayout()
        
        htmlText = f"""
        <h3>{plugin_nm} - release {version}</h3>
        Created by M. Alberti (alberti.m65@gmail.com).
        <br /><br />Licensed under the terms of GNU GPL 3.
        """
               
        aboutQTextBrowser = QTextBrowser(self)
        aboutQTextBrowser.insertHtml(htmlText)         
        aboutQTextBrowser.setMinimumSize(400, 200)
        dialog_layout.addWidget(aboutQTextBrowser)

        self.setLayout(dialog_layout)                    

        self.setWindowTitle('{} about'.format(plugin_nm))


