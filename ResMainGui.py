
from .AsterDialog import *
from .FiltersDialog import *
from .AboutDialog import *

from .resources import *


class MainGui(object):

    def __init__(self, interface):

        self.plugin_folder = os.path.dirname(__file__)
        self.config_fldrpth = os.path.join(
            self.plugin_folder,
            config_fldr)

        plugin_config_file = os.path.join(
            self.config_fldrpth,
            plugin_params_flnm)

        dPluginParams = read_yaml(plugin_config_file)

        self.version = dPluginParams["version"]
        self.tools = dPluginParams["tools"]

        self.filters_toolpars = self.tools["filters_tool_params"]
        self.aster_toolpars = self.tools["aster_tool_params"]
        self.about_toolpars = self.tools["about_dlg_params"]

        self.bAsterWidgetOpen = False
        self.bFilterWidgetOpen = False

        self.interface = interface
        self.main_window = self.interface.mainWindow()
        self.canvas = self.interface.mapCanvas()

    def initGui(self):

        self.aster_geoproc = make_qaction(
            tool_params=self.aster_toolpars,
            plugin_nm=plugin_nm,
            icon_fldr=icon_fldr,
            parent=self.main_window)
        self.aster_geoproc.triggered.connect(self.RunAsterGeoproc)
        self.interface.addPluginToMenu(
            plugin_nm,
            self.aster_geoproc)

        self.filters_geoproc = make_qaction(
            tool_params=self.filters_toolpars,
            plugin_nm=plugin_nm,
            icon_fldr=icon_fldr,
            parent=self.main_window)
        self.filters_geoproc.triggered.connect(self.RunFiltersGeoproc)
        self.interface.addPluginToMenu(
            plugin_nm,
            self.filters_geoproc)

        self.plugin_about = make_qaction(
            tool_params=self.about_toolpars,
            plugin_nm=plugin_nm,
            icon_fldr=icon_fldr,
            parent=self.main_window
        )
        self.plugin_about.triggered.connect(self.RunPluginAbout)
        self.interface.addPluginToMenu(
            plugin_nm,
            self.plugin_about
        )

        self.geoprocessings = [
            self.aster_geoproc,
            self.filters_geoproc,
            self.plugin_about
        ]

    def RunAsterGeoproc(self):

        if self.bAsterWidgetOpen:
            return

        AsterDockWidget = QDockWidget(
            "{} - {}".format(plugin_nm, self.aster_toolpars["tool_nm"]),
            self.interface.mainWindow())
        AsterDockWidget.setAttribute(Qt.WA_DeleteOnClose)
        AsterDockWidget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)

        self.AsterMainQwidget = AsterMainWidget(
            tool_nm=self.aster_toolpars["tool_nm"],
            canvas=self.canvas,
            plugin_qaction=self.aster_geoproc
        )
        AsterDockWidget.setWidget(self.AsterMainQwidget)
        AsterDockWidget.destroyed.connect(self.AsterWinCloseEvent)
        self.interface.addDockWidget(Qt.RightDockWidgetArea, AsterDockWidget)

        self.bAsterWidgetOpen = True

    def RunFiltersGeoproc(self):

        if self.bFilterWidgetOpen:
            return

        FiltersDockWidget = QDockWidget(
            "{} - {}".format(plugin_nm, self.filters_toolpars["tool_nm"]),
            self.interface.mainWindow())
        FiltersDockWidget.setAttribute(Qt.WA_DeleteOnClose)
        FiltersDockWidget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)

        self.FiltersMainQwidget = FiltersMainWidget(
            tool_nm=self.filters_toolpars["tool_nm"],
            canvas=self.canvas,
            plugin_qaction=self.filters_geoproc
        )
        FiltersDockWidget.setWidget(self.FiltersMainQwidget)
        FiltersDockWidget.destroyed.connect(self.FiltersWinCloseEvent)
        self.interface.addDockWidget(Qt.RightDockWidgetArea, FiltersDockWidget)

        self.bFilterWidgetOpen = True

    def RunPluginAbout(self):

        qgsurf_about_dlg = AboutDialog(
            version=self.version)

        qgsurf_about_dlg.show()
        qgsurf_about_dlg.exec_()

    def AsterWinCloseEvent(self, event):

        self.bAsterWidgetOpen = False

    def FiltersWinCloseEvent(self, event):

        self.bFilterWidgetOpen = False

    def info(self, msg):

        QMessageBox.information(
            None,
            plugin_nm,
            msg
        )

    def warn(self, msg):

        QMessageBox.warning(
            None,
            plugin_nm,
            msg
        )

    def unload(self):

        for geoprocessing in self.geoprocessings:

            self.interface.removePluginMenu(
                plugin_nm,
                geoprocessing)
