"""
/***************************************************************************
 res - Remote Earth Science - using Aster data in QGIS for detecting lithological components.
 A Python plugin for QGIS 3.

                              -------------------
        begin                : 2020-12-24
        copyright            : (C) 2020 by Mauro Alberti
        email                : alberti.m65@gmail.com
        
 ***************************************************************************/

# licensed under the terms of GNU GPL 3

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import sys
import os

sys.path.append(f'{os.path.dirname(__file__)}/embedded')

def classFactory( iface ):    
    from .ResMainGui import MainGui
    return MainGui(iface)



